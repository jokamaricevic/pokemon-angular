
export interface SimplePokemon {
  name: string;
  url: string;
}

export interface MainResponse {
  results: Array<SimplePokemon>;
}

interface Sprites {
  front_default: string;
}

export interface Pokemon {
  name: string;
  sprites: Sprites;
  species: SimplePokemon;
}

class Chain {
  species: SimplePokemon;
  evolves_to: Array<Chain>
}

export interface EvolutionChain {
  chain: Chain;
}

export interface ChainUrl {
  url: string;
}

export interface Species {
  evolution_chain: ChainUrl;
  id: number;
  name: string;
}
