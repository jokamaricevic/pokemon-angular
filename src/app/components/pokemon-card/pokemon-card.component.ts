import {Component, OnInit, Input, Inject} from '@angular/core';
import {PokemonService} from "../../services/pokemon.service";
import {Observable} from "rxjs";
import {MainResponse} from "../../Pokemon";
import {MainComponent} from "../main/main.component";

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.scss']
})
export class PokemonCardComponent implements OnInit {
  @Input() pokemonName: string;
  @Input() isImageLoading: boolean;
  @Input() image: any;
  evolveUrl: string;

  constructor(private pokemonService: PokemonService,
              @Inject(MainComponent) private parent: MainComponent) { }

  ngOnInit(): void {
    this.getRandomPokemon()

  }

  getRandomPokemon(): Observable<MainResponse> {
    let response = this.pokemonService.getRandomPokemon();
    response.subscribe(x => {
      let pokemon = x.results[Math.floor(Math.random() * x.results.length)];
      this.showPokemonImage(pokemon.url);
    });
    return response;
  }

  showPokemonImage(url: string) {
    this.isImageLoading = true;
    this.pokemonService.getPokemon(url).subscribe(x => {
      this.setImage(this.pokemonService.getImage(x.sprites.front_default));
      this.setEvolveButton(x.species.url);
      this.pokemonName = x.name;
    });
  }

  setImage(blobResponse: Observable<Blob>): void{
    blobResponse.subscribe(data => {
      this.createImageFromBlob(data);
      this.isImageLoading = false;
      },
        error => {
      console.log(error);
      this.isImageLoading = false;
    });
  }

  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => this.image = reader.result , false);
    if (image) reader.readAsDataURL(image);
  }

  private setEvolveButton(url: string) {
    this.pokemonService.getSpecies(url).subscribe(x => {
      this.pokemonService.getChain(x.evolution_chain.url).subscribe(evolution => {
        let chain = evolution.chain;
        let currentSpeciesName = chain.species.name;
        while(currentSpeciesName != x.name) {
          chain = chain.evolves_to[0];
          currentSpeciesName = chain.species.name;
        }
        if (chain.evolves_to.length > 0) {
          this.parent.canEvolve = true;
          this.evolveUrl = chain.evolves_to[0].species.url;
        } else {
          this.parent.canEvolve = false;
        }
      })
    });
  }

  evolve() {
    if (this.evolveUrl) this.pokemonService.getSpecies(this.evolveUrl).subscribe( x => {
      this.showPokemonImage("https://pokeapi.co/api/v2/pokemon/" + x.id)
    });
  }
}
