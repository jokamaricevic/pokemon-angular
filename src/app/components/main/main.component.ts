import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {PokemonCardComponent} from "../pokemon-card/pokemon-card.component";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  @ViewChild(PokemonCardComponent) pokemonCard: PokemonCardComponent;
  @Input() canEvolve: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

  getRandomPokemon() {
    this.pokemonCard.getRandomPokemon();
  }

  evolve() {
    this.pokemonCard.evolve()
  }
}
