import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import {EvolutionChain, MainResponse, Pokemon, Species} from '../Pokemon'

@Injectable({
  providedIn: 'root'
})

export class PokemonService {
  private baseUrl = 'https://pokeapi.co/api/v2/'


  constructor(private http: HttpClient) {
  }

  getRandomPokemon(): Observable<MainResponse> {
    return this.http.get<MainResponse>(this.baseUrl + "pokemon/");
  }

  getPokemon(url: string): Observable<Pokemon> {
    return this.http.get<Pokemon>(url);
  }

  getImage(url: string): Observable<Blob> {
    return this.http.get(url, {responseType: 'blob'});
  }

  getSpecies(url: string): Observable<Species> {
    return this.http.get<Species>(url);
  }

  getChain(url: string): Observable<EvolutionChain> {
    return this.http.get<EvolutionChain>(url);
  }
}
